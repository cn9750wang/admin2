/**
 * index.html页面会用到的脚本和依赖
 * @author wwy
 */
layui.config({
	base: '../static/' //假设这是你存放拓展模块的根目录
}).extend({ //设定模块别名
  navbar: 'lib/layui-lib/navbar',
  common: 'lib/layui-lib/common',
  navTab: 'assets/js/navTab'
});

/** layui方式引入jquery和element依赖 */
layui.use(['jquery', 'element'], function() {
	var $ = layui.$;
	var element = layui.element;

	$(function() {
		//左侧菜单
		var leftNav = $("#left-nav");
		//主体部分
		var rightBody = $("#right-body");
		//是否显示左侧菜单,默认显示
		var leftNavShow = true;
		var onFullScreen = false;
		//是否全屏 默认不是全屏
		var fullScreenStatus = false;

		/**
		 * 隐藏左侧菜单栏
		 */
		function hideNav(hide) {
			leftNavShow = typeof hide === 'boolean' ? hide : !leftNavShow;
			if (leftNavShow) {
				$("#show-left-nav-btn").attr("title", "收起菜单");
				$("#show-left-nav-btn .layui-icon").addClass("layui-icon-shrink-right").removeClass("layui-icon-spread-left");
				leftNav.removeClass("hide");
				rightBody.removeClass("full");
				$("#left-nav-mask").addClass("active");
			} else {
				$("#show-left-nav-btn").attr("title", "展开菜单");
				$("#show-left-nav-btn .layui-icon").addClass("layui-icon-spread-left").removeClass("layui-icon-shrink-right");
				leftNav.addClass("hide");
				rightBody.addClass("full");
				$("#left-nav-mask").removeClass("active");
			}
		}
		//绑定隐藏菜单按钮事件
		$("#show-left-nav-btn").click(function() {
			hideNav();
		});
		$("#left-nav-mask").click(function() {
			hideNav(false);
		});

		//绑定全屏 / 退出全屏事件
		$("#full-screen").click(function() {
			if (!fullScreenStatus) {
				$(this).attr("title", "退出全屏");
				requestFullScreen();
			} else {
				$(this).attr("title", "全屏");
				exitFullscreen();
			}
		});

		resizeWindow();

		function resizeWindow() {
			setTimeout(function() {
				var width = $(window).width();
				if (!onFullScreen)
					hideNav(width > 1200);
			}, 50);
		}

		//浏览器窗口大小变化时 
		$(window).resize(resizeWindow);


		/** 监听全屏事件 */
		document.addEventListener("fullscreenchange", function(e) {
			fullScreenStatus = !fullScreenStatus;
			if (fullScreenStatus) {
				onFullScreen = true;
				$("#full-screen i.layui-icon").addClass("layui-icon-screen-restore").removeClass("layui-icon-screen-full");
			} else {
				$("#full-screen i.layui-icon").addClass("layui-icon-screen-full").removeClass("layui-icon-screen-restore");
				setTimeout(function() {
					onFullScreen = false;
				}, 100);
			}
		});

		//进入全屏
		function requestFullScreen() {
			var docElm = document.documentElement;
			//W3C  
			if (docElm.requestFullscreen) {
				docElm.requestFullscreen();
			}
			//FireFox  
			else if (docElm.mozRequestFullScreen) {
				docElm.mozRequestFullScreen();
			}
			//Chrome等  
			else if (docElm.webkitRequestFullScreen) {
				docElm.webkitRequestFullScreen();
			}
			//IE11
			else if (elem.msRequestFullscreen) {
				elem.msRequestFullscreen();
			}
		}

		//退出全屏
		function exitFullscreen() {
			var docElm = document;

			//W3C  
			if (docElm.exitFullscreen) {
				docElm.exitFullscreen();
			}
			//FireFox  
			else if (docElm.mozCancelFullScreen) {
				docElm.mozCancelFullScreen();
			}
			//Chrome等  
			else if (docElm.webkitCancelFullScreen) {
				docElm.webkitCancelFullScreen();
			}
			//IE11
			else if (elem.msCancelFullScreen) {
				elem.msCancelFullScreen();
			}
		}

	});

});

layui.use(['jquery','navbar'], function() {
	var $ = layui.$,
		navbar = layui.navbar();

	var data = [
		{
			"title": "首页",
			"icon": "layui-icon-home",
			"href": "homepage.html"
		},
		{
			"title": "一级导航",
			"icon": "layui-icon-username",
			"children": [
				{
					"title": "二级导航",
					"icon": "layui-icon-username"
				},
			]
		},
		{
			"title": "一级导航",
			"icon": "fa-stop-circle",
			"href": "http://www.baidu.com"

		},
		{
			"title": "一级导航",
			"icon": "fa-stop-circle",
			"href": "http://www.baidu.com"

		},
		{
			"title": "数据表格",
			"icon": "fa-stop-circle",
			"href": "table.html"
		}
	];
	
	navbar.set({
        elem: '#left-nav-list',
        data: data
    }).render();
	
});

/**
 * 设置点击菜单,切换到对应tab
 */
layui.use(['jquery','navTab'],function(){
	var $ = layui.$,
	navTab = layui.navTab;
	var filter = 'index-tabs';
	
	/** 监听左侧菜单点击事件 */
	$("#left-nav").on("click","#left-nav-list a",function(event){
		var ele = $(this);
		var href = ele.attr("href");
		var title = ele.attr("title");
		navTab.add(filter,{
			title:title,
			href:href
		});
		return false;
	});
	
	/** 监听刷新点击事件 */
	$("#reload-tab-page").click(function(event){
		navTab.reload(filter);
	});
	
});