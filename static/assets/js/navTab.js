layui.define(['jquery','element'],function(exports){
	var $ = layui.$,
		element = layui.element;

	var obj = {
		/**
		 * 添加页面到指定选项卡页面中
		 * @param filter 选项卡filter
		 * @param options 选项
		 */
		add:function(filter,options){
			if(!filter ||typeof filter != 'string'){
				return;
			}
			var setting = {
				//url链接
				href:undefined,
				id:undefined,
				//标题
				title:"新选项卡",
				//是否同一url打开多个页面
				multiPage:false,
				//允许运行脚本
				script:false,
				//是否打开页面
				open:true
			}
			$.extend(setting,options);
			//无href,返回
			if(!setting.href){
				return;
			}
			setting.id = setting.id || setting.href;
			//是否允许多页面
			if(setting.multiPage){
				//允许多页面直接添加
				setting.id = setting.id + "?r="+Math.random();
				addPage(filter,setting);
				return;
			}
			var tabs = getAllTabs(filter);
			var changeToId;
			for(var i in tabs){
				var temp = tabs[i];
				if(temp.id == setting.id){
					changeToId = temp.id;
					break;
				}
			}
			if(changeToId){
				element.tabChange(filter, changeToId);
			}else{
				addPage(filter,setting);
			}
		},
		/**
		 * 获得当前所在的页面
		 * @param filter 过滤器
		 */
		getActive:function(filter){
			var li = $("[lay-filter='"+ filter +"'] ul li.layui-this");
			var page = $("[lay-filter='"+ filter +"'] .layui-tab-content .layui-show");
			var obj = {
				pageContent:page
			}
			$.extend(obj,getTabMsg(li));
			return obj;
		},
		/**
		 * 刷新当前标签页
		 * @param filter 过滤器
		 */
		reload:function(filter){
			var active = obj.getActive(filter);
			if(isScript(active.id)){
				
			}else{
				var iframe = active.pageContent.find("iframe");
				iframe.attr("src",iframe.attr("src"))
			}
		}
	};
	
	/**
	 * 添加页面到选项卡中
	 * @param filter 过滤器
	 * @param options 选项
	 */
	function addPage(filter,options){
		var html
		if(isScript(options.href)){
			if(options.script){
				throw new Error("禁止执行脚本");
				return;
			}
			var href = options.href.replace("javascript:","");
			html = eval(href);
			if(!html){
				return;
			}
		}else{
			html = $("<iframe></iframe>").attr("src",options.href)[0].outerHTML;
		}
		element.tabAdd(filter, {
			title: options.title,
			content: html,
			id: options.id
		});
		if(options.open){
			element.tabChange(filter, options.id);
		}
	}
	
	/**
	 * 获得指定选项卡的选项
	 * @param filter 过滤器
	 */
	function getAllTabs(filter){
		var li = $("[lay-filter='"+ filter +"'] ul li");
		var list = [];
		if(li && li.length > 0){
			li.each(function(k,v){
				v = $(v);
				var item = getTabMsg(v);
				list.push(item);
			});
		}
		return list;
	}
	
	function getTabMsg(liEle){
		return {
			title:liEle.attr("title"),
			id:liEle.attr("lay-id"),
			li:liEle
		}
	}
	
	function getText(ele){
		var text = "";
		ele.contents().each(function(){
			if(this.nodeType === 3){
				text += this.wholeText;
			}
		});
		return text;
	}
	
	/** 链接是否为脚本 */
	function isScript(href){
		return /^javascript:.+/.test(href);
	}
	
	exports('navTab',obj);
});